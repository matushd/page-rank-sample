package com.emde.repository

import scalikejdbc._
import scalikejdbc.config._
import org.joda.time._
import com.emde.model.PageRank

object PageRankRepository {
  
   def create(url: String, rank: Double, title: String)(implicit s: DBSession = AutoSession): PageRank = {
    val date: DateTime = DateTime.now()
    val column = PageRank.column
    val id = sql"insert into ${PageRank.table} (${column.url}, ${column.rank}, ${column.date}, ${column.title}) values (${url}, ${rank}, ${date}, ${title})"
      .updateAndReturnGeneratedKey.apply().toInt
    PageRank(id, url, rank, date, title)
  }
  
  def find(url: String)(implicit s: DBSession = AutoSession): PageRank = {
    sql"SELECT * FROM pagerank WHERE pagerank_url = ${url} LIMIT 1".map(rs => PageRank(rs))
        .list.apply().iterator.next()
  }
}