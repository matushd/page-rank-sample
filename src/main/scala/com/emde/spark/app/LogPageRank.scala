package com.emde.spark.app

import org.apache.spark.{SparkConf, SparkContext}
import com.emde.pagerank.Service

/**
 * Usage:
 * sbt assembly && /usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.LogPageRank --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar /home/emde/logs/event.log
 */
object LogPageRank {
  
  def main(args: Array[String]): Unit = {
    
    if (args.length < 1) {
      System.err.println(s"""
        |Usage: LogPageRank <file>
        |  <file> PageRank input data file
        |
        """.stripMargin)
      System.exit(1)
    }

    // Create the context
    val sparkConf = new SparkConf().setAppName("FileWordCount")
    val ssc = new SparkContext(sparkConf)

    val lines = ssc.textFile(args(0))
    Service.createPageRank(lines, ssc)
  }
}