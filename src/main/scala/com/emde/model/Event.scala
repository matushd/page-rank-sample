package com.emde.model

import scala.collection.Map

class Event(json: Map[String, Any], ident: Long = 0) {
  val data = new EventData(json("data").asInstanceOf[Map[String, Any]])
  val hash = json("hash")
  val sessionId = json("sessionId")
  val referer = json("referer")
  val userId = json("userId")
  val title = json("title")
  val search = json("search")
  val path = json("path")+search.toString()
  
  override def toString(): String = "(path: " + path + ", data: " + data +
  ", hash: " + hash + ", sessionId: " + sessionId + ", referer:" + referer +
  ", userId: " + userId + ", title: " + title + ", search: " + search + ")";
}