package com.emde.spark.app

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{StreamingContext, Seconds}
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.kafka.KafkaUtils
import com.emde.pagerank.Service

/**
 * Usage:
 * sbt assembly && /usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.KafkaPageRank --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar localhost:9092 events
 */
object KafkaPageRank {
  
  def main(args: Array[String]) {
    if (args.length < 2) {
      System.err.println(s"""
        |Usage: KafkaPageRank <brokers> <topics>
        |  <brokers> is a list of one or more Kafka brokers
        |  <topics> is a list of one or more kafka topics to consume from
        |
        """.stripMargin)
      System.exit(1)
    }
    val Array(brokers, topics) = args

    // Create context with 24h batch interval
    val sparkConf = new SparkConf().setAppName("KafkaPageRank")
    val ssc = new StreamingContext(sparkConf, Seconds(86400))

    // Create direct kafka stream with brokers and topics
    val topicsSet = topics.split(",").toSet
    val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers)
    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc, kafkaParams, topicsSet)

    // Get the lines from messages
    val lines = messages.map(_._2)
    lines.foreachRDD { x => if(!x.isEmpty()) Service.createPageRank(x, ssc.sparkContext) }
    
    ssc.start()
    ssc.awaitTermination()
  }
}