# Kafka
```
/usr/local/kafka/bin/zookeeper-server-start.sh /usr/local/kafka/config/zookeeper.properties
/usr/local/kafka/bin/kafka-server-start.sh /usr/local/kafka/config/server.properties
```

## Create topic
```
/usr/local/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic events
```

## Topic list
```
/usr/local/kafka/bin/kafka-topics.sh --list --zookeeper localhost:2181
```

## Check
```
/usr/local/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic events
/usr/local/kafka/bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic events --from-beginning
```

# Spark:
```
sbt assembly
```

## PageRank:
### File
```
/usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.LogPageRank --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar /home/emde/logs/event.log
```
### Kafka:
```
/usr/local/spark-1.6.1/bin/spark-submit --class com.emde.spark.app.KafkaPageRank --master local[2] target/scala-2.10/com-emde-spark-assembly-1.0.jar localhost:9092 events
```
