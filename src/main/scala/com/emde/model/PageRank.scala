package com.emde.model

import org.joda.time._
import scalikejdbc._

case class PageRank(id: Integer, url: String, rank: Double, date: DateTime, title: String)

object PageRank extends SQLSyntaxSupport[PageRank] {
  
  override val tableName = "pagerank"
  
  override val columns = Seq("pagerank_id", "pagerank_url", "pagerank_value", "pagerank_date", "pagerank_title")
  
  override val nameConverters = Map(
      "^id$" -> "pagerank_id",
      "^url$" -> "pagerank_url",
      "^rank$" -> "pagerank_value",
      "^date$" -> "pagerank_date",
      "^title$" -> "pagerank_title"
      )
  
  def apply(rs: WrappedResultSet) = new PageRank(rs.int("pagerank_id"), rs.string("pagerank_url"),
      rs.double("pagerank_value"), rs.jodaDateTime("pagerank_date"), rs.string("pagerank_title"))
}