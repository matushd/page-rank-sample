package com.emde.pagerank

import org.apache.spark.graphx._
import scala.util.parsing.json.JSON
import org.apache.spark.rdd.RDD
import com.emde.repository.PageRankRepository
import org.apache.spark.{SparkContext}
import scalikejdbc.config.DBs
import com.emde.model.Event

object Service {
  
  def createPageRank(lines: RDD[String], ssc: SparkContext) = {
    //Prepare event logs
    val split = lines.flatMap(line => {
      line.split(" ~ ")
    })
    val logs = split.map({ x => JSON.parseFull(x) }).filter({ _.isDefined })
    .map { x => new Event(x.get.asInstanceOf[Map[String, Any]]) }
    
    //Url title map
    val urlsTitle = logs.map { x => Map[String, String](x.path.toString() -> x.title.toString()) }
      .reduce({case(x,y) => x ++ y})
    
    
    val urlsPairs = logs.map { x => Array(x.path.toString(), x.referer.toString()) }
    val urlsIds = urlsPairs.reduce({case(x,y) => x ++ y}).distinct
    
    //Prepare urls
    val urls: RDD[(VertexId, String)] = ssc.parallelize(urlsIds.zipWithIndex.map { case (x, i) => (i.toLong, x) })

    //Prepare url relations
    val rels = urlsPairs.map({ x => Array(Edge(urlsIds.indexOf(x(0)), urlsIds.indexOf(x(1)), null)) })
        .reduce({case(x,y) => x ++ y})
    val urlsRelations: RDD[Edge[Null]] = ssc.parallelize(rels)
        
    //Prepare urls graph
    val graph = Graph(urls, urlsRelations)
    
    // Run PageRank
    val ranks = graph.pageRank(0.0001).vertices
    
    //Ranks by url
    val ranksByUrl = urls.join(ranks).map {
      case (id, (url, rank)) => (url, rank, urlsTitle.getOrElse(url, ""))
    }
    
    //Import to db
    DBs.setupAll()
    ranksByUrl.foreach(
        r => PageRankRepository.create(r._1, r._2, r._3)
    )
    DBs.closeAll()
  }
}