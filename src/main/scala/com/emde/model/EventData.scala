package com.emde.model

import scala.collection.Map

class EventData(json: Map[String, Any]) {
  
  val name = json.getOrElse("name", "")
  
  override def toString(): String = "(name: " + name + ")"
}